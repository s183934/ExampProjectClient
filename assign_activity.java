package dtu.software.acceptance_tests;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.softwarehouse.Employee;
import dtu.softwarehouse.OperationNotAllowedException;

public class assign_activity{


	
	private ErrorMessageHolder errorMessageHolder;
	private String activity = "";
	private String employeeID = "";
	private Employee employee;
	
	public assign_activity(Employee employee, ErrorMessageHolder errorMessageHolder) {
		this.employee = employee;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@Given("there is an activity {string}")
	public void thereIsAnActivity(String string) {
	    this.activity = string;
	    assertTrue(employee.activityExist(activity));
	    
	}


	@Given("the employee {string} is not already assigned to the activity")
	public void theEmployeeIsNotAlreadyAssignedToTheActivity(String string) {
	    employeeID = string;
	    assertFalse(employee.activityAlreadyAssigned(employeeID, activity));
	}

	@When("I assign the activity")
	public void iAssignTheActivity() throws OperationNotAllowedException {
	    try{ 
	    	employee.assignActivity(employeeID, activity);
	    }
	    catch(OperationNotAllowedException e) {
	    	errorMessageHolder.setErrorMessage(e.getMessage());
	    }
	}

	@Then("the activity is assigned to the employee")
	public void theActivityIsAssignedToTheEmployee() {
	    assertTrue(employee.activityAlreadyAssigned(employeeID, activity));
	}
	
	@Given("there is no activity {string}")
	public void thereIsNoActivity(String string) {
		activity = string;
	    assertFalse(employee.activityExist(activity));
	}

	@Then("i get the error message {string}")
	public void iGetTheErrorMessage(String errorMessage) {
		assertThat(errorMessageHolder.getErrorMessage(), is(equalTo(errorMessage)));
	}

	
	
	



}
