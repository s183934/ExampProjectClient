package dtu.software.acceptance_tests;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.softwarehouse.Employee;

public class add_employee{


	
	private ErrorMessageHolder errorMessageHolder;
	String user = "";
	private Employee employee;
	
	public add_employee(Employee employee, ErrorMessageHolder errorMessageHolder) {
		this.employee = employee;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	@When("I add the employee {string}")
	public void iAddTheEmployee(String string) {
		try {
			employee.makeNewEmployee(string);
		}catch(Exception e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
	    
	   
	}
	@Then("I get an error message {string}")
	public void iGetAnErrorMessage(String errorMessage) {
		assertThat(errorMessageHolder.getErrorMessage(), is(equalTo(errorMessage)));
	}
	
	@Then("the employee {string} exist")
	public void theEmployeeExist(String name) {
	    assertTrue(employee.employeeAndIDExists(name));
	}
	



}
