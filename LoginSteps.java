package dtu.software.acceptance_tests;

import dtu.softwarehouse.Employee;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	
	private Employee employee;
	
	public LoginSteps(Employee employee) {
		this.employee = employee;
	}
	
	@Given("that the employee is not logged in")
	public void thatTheEmployeeIsNotLoggedIn() throws Exception {
		assertFalse(employee.isLoggedIn());
	}
	
	@Given("the employee with ID {string} exist")
	public void theEmployeeWithIDExist(String string) {
		assertTrue(employee.employeeExists(string));
	}


	
	@Then("the employee {string} logs in")
	public void theUserLogsIn(String string) throws Exception{
		try {
			employee.setLoggedIn(string);
		}catch (Exception e){
			
		}
	}


	@And("the employee is logged in")
	public void theEmployeeIsLoggedIn() throws Exception{
		assertTrue(employee.isLoggedIn());
	}
	

	@Given("the employee {string} doesnt exist")
	public void theUserDoesNotExist(String string) {
		assertFalse(employee.employeeExists(string));
	}

	@Then("the employee is not logged in")
	public void theEmployeeIsNotLoggedIn() {
		assertFalse(employee.isLoggedIn());
	}
	


	

}
