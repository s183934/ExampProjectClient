package dtu.software.acceptance_tests;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.softwarehouse.Employee;
import dtu.softwarehouse.OperationNotAllowedException;

public class register_workinghours{


	
	private ErrorMessageHolder errorMessageHolder;
	private String time;
	private String activity;
	private Employee employee;
	
	public register_workinghours(Employee employee, ErrorMessageHolder errorMessageHolder) {
		this.employee = employee;
		this.errorMessageHolder = errorMessageHolder;
	}
	

	@Given("I have an activity {string}")
	public void iHaveAnActivity(String string) {
	   activity = string;
	}
	
	@When("I register {string} working hours")
	public void iRegisterWorkingHours(String string) {
		time = string;
		try {
			employee.registerWorkingHours(activity, time,employee.getLoggedIn());
		}
	    catch(OperationNotAllowedException e) {
	    }
	}

	@Then("the working hours are registered with ID {string} and activity {string}")
	public void theWorkingHoursAreRegisteredWithIDAndActivity(String userID, String activity) {
		assertTrue(employee.checkLastRegistered(userID, activity, time));
	}



}
