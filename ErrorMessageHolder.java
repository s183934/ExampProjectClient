package dtu.software.acceptance_tests;

public class ErrorMessageHolder {
	private String errorMessage;

	String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
}
