package dtu.software.acceptance_tests;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtu.softwarehouse.Employee;

public class create_activity{


	
	private ErrorMessageHolder errorMessageHolder;
	String activity = "";
	private Employee employee;
	
	public create_activity(Employee employee, ErrorMessageHolder errorMessageHolder) {
		this.employee = employee;
		this.errorMessageHolder = errorMessageHolder;
	}
	
	
	@Given("there is no activity with the name {string}")
	public void thereIsNoActivityWithTheName(String string) {
		this.activity = string;
	    assertFalse(employee.activityExist(string));
	}
	
	@When("I create the activity")
	public void iCreateTheActivity() {
		try {
			employee.makeNewActivity(activity);
			
		}catch(Exception e) {
			errorMessageHolder.setErrorMessage(e.getMessage());
		}
		
	}
	
	@Then("there is created an activity with the name {string}")
	public void thereIsCreatedAnActivityWithTheName(String string) {
		assertTrue(employee.activityExist(activity));
	}
	
	@Given("there is already an activity with name {string}")
	public void thereIsAlreadyAnActivityWithName(String string) {
		this.activity = string;
		assertTrue(employee.activityExist(activity));
	}


	@Then("I get a error message {string}")
	public void iGetAErrorMessage(String errorMessage) throws Exception{
		assertThat(errorMessageHolder.getErrorMessage(), is(equalTo(errorMessage)));
	}
	


	@Then("the activity is deleted")
	public void theActivityIsDeleted() {
		assertFalse(employee.activityExist(activity));
	}
	
	
	



}
